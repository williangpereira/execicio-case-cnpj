package br.com.empresas.cadastro.services;

import br.com.empresas.cadastro.models.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class CadastroProducer {

    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    public void enviarAoKafka(Empresa empresa) {
        producer.send("spec3-willian-garcia-2", empresa);
    }
}
