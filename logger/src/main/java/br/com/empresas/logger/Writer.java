package br.com.empresas.logger;

import br.com.empresas.cadastro.models.EmpresaRetorno;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;

@Service
public class Writer {

    public static void generateCsvFile(String sFileName, EmpresaRetorno empresa) {
        try {
            FileWriter writer = new FileWriter(sFileName, true);

            writer.append(empresa.getCnpj());
            writer.append(empresa.getStatus());

            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
