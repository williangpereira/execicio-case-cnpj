package br.com.empresas.validador;

import br.com.empresas.cadastro.models.Empresa;
import br.com.empresas.cadastro.models.EmpresaRetorno;
import br.com.empresas.validador.clients.RetornoReceita;
import br.com.empresas.validador.clients.ValidaCNPJ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class EmpresaConsumer {

    @Autowired
    private ValidaCNPJ validaCNPJ;

    @Autowired
    private Producer producer;


    @KafkaListener(topics = "spec3-willian-garcia-2", groupId = "Willian-2")
    public void receber(@Payload Empresa empresa) {
        EmpresaRetorno empresaRetorno = new EmpresaRetorno();
        empresaRetorno.setCnpj(empresa.getCnpj());
        RetornoReceita retornoReceita = validaCNPJ.consultarCNPJ(empresa.getCnpj());
        if (retornoReceita.getCapital_social() < 1000000) {
            empresaRetorno.setStatus("Recusado!");
        } else{
            empresaRetorno.setStatus("Aceito!");
        }
        producer.enviarAoKafka2(empresaRetorno);
    }
}
