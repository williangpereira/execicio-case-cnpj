package br.com.empresas.validador.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Empresa com capital inferior a 1 milhão")
public class CNPJInvalidoException extends RuntimeException{
}
