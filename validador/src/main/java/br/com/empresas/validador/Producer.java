package br.com.empresas.validador;

import br.com.empresas.cadastro.models.EmpresaRetorno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Producer {

    @Autowired
    private KafkaTemplate<String, EmpresaRetorno> producer;

    public void enviarAoKafka2(EmpresaRetorno empresaRetorno) {
        producer.send("spec3-willian-garcia-3", empresaRetorno);
    }
}